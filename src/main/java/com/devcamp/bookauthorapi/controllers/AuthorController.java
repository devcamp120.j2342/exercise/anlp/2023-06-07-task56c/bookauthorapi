package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.services.AuthorService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AuthorController {
    @Autowired
    AuthorService authorService;
    @GetMapping("/author-info")
    public Author getAuthorByEmail(@RequestParam(name="email", required = true) String email) {
        return authorService.getAuthorByEmail(email);
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorsByGender(@RequestParam(name="gender", required = true) Character gender) {
        return authorService.getAuthorsByGender(gender);
    }
}
