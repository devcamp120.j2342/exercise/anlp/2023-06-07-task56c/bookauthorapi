package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.bookauthorapi.models.Book;

@Service
public class BookService {
    @Autowired
    AuthorService authorService;

    public ArrayList<Book> getAllBooks(){
        Book book1 = new Book("ABC", authorService.authorList1(), 104500, 1);
        Book book2 = new Book("Toán cao cấp",  authorService.authorList2(), 75000, 3);
        Book book3 = new Book("Vật lý đại cương",  authorService.authorList3(), 125000, 2);

        ArrayList<Book> arrbooks = new ArrayList<Book>();
        arrbooks.add(book1);
        arrbooks.add(book2);
        arrbooks.add(book3);
    
        return arrbooks;
    }

    public ArrayList<Book> getBooksByQuantity(@RequestParam(name="quantityNumber", required = true) int quantityNumber){
        ArrayList<Book> allBook = getAllBooks();
        ArrayList<Book> findBook = new ArrayList<Book>();

        for (Book book : allBook) {
            if (book.getQty() >= quantityNumber) {
                findBook.add(book);
            }
        }
        return findBook;
    }
}
